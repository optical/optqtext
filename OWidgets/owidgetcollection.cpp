#include "ofilewidgetplugin.h"
#include "owidgetcollection.h"

OWidgetCollection::OWidgetCollection(QObject *parent)
    : QObject(parent)
{
    m_widgets.append(new OFileWidgetPlugin(this));
}

QList<QDesignerCustomWidgetInterface*> OWidgetCollection::customWidgets() const
{
    return m_widgets;
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(owidgetcollectionplugin, OWidgetCollection)
#endif // QT_VERSION < 0x050000
