HEADERS += \
    $$PWD/Widget/ofilewidget.h \
    $$PWD/Plugin/ofilewidgetplugin.h

SOURCES += \
    $$PWD/Widget/ofilewidget.cpp \
    $$PWD/Plugin/ofilewidgetplugin.cpp

FORMS += \
    $$PWD/Widget/ofilewidget.ui

INCLUDEPATH += $$PWD/Widget
INCLUDEPATH += $$PWD/Plugin
