#ifndef OFILEWIDGET_H
#define OFILEWIDGET_H

#include <QWidget>
#include <QFileDialog>

namespace Ui {

/**
\page index
\title Title of the page

Contents of the page
*/

/*!
    \class OFileWidget
    \brief The QVector3D class represents a vector or vertex in 3D space.
    \since 4.6
    \ingroup painting-3D

    Vectors are one of the main building blocks of 3D representation and
    drawing.  They consist of three coordinates, traditionally called
    x, y, and z.

    The QVector3D class can also be used to represent vertices in 3D space.
    We therefore do not need to provide a separate vertex class.

    \b{Note:} By design values in the QVector3D instance are stored as \c float.
    This means that on platforms where the \c qreal arguments to QVector3D
    functions are represented by \c double values, it is possible to
    lose precision.
*/

class OFileWidget;
}

class OFileWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OFileWidget(QWidget *parent = 0);
    ~OFileWidget();

signals:
    void onSaveClicked(const QString &path);
    void onOpenClicked(const QString &path);

public:
    void setLabel(const QString &text);
    void setPath(const QString &text);
    void setSaveButtonText(const QString &text);
    void setOpenButtonText(const QString &text);
    void setOpenFileCaption(const QString &value);
    void setSaveFileCaption(const QString &value);
    void setOpenFileDir(const QString &value);
    void setSaveFileDir(const QString &value);
    void setOpenFileFilter(const QString &value);
    void setOpenFileOption(const QFileDialog::Option &value);
    void setSaveFileOption(const QFileDialog::Option &value);

public:
    QString getLabel() const;
    QString getPath() const;
    bool isSaveButtonVisible() const;
    bool isOpenButtonVisible() const;
    bool isLineEditVisible() const;
    bool isSaveButtonEnabled() const;
    bool isOpenButtonEnabled() const;
    bool isLineEditEnabled() const;
    QString getOpenFileCaption() const;
    QString getSaveFileCaption() const;
    QString getOpenFileDir() const;
    QString getSaveFileDir() const;
    QString getOpenFileFilter() const;
    QFileDialog::Option getOpenFileOption() const;
    QFileDialog::Option getSaveFileOption() const;

public slots:
    void setSaveButtonVisible(bool val);
    void setOpenButtonVisible(bool val);
    void setLineEditVisible(bool val);
    void setSaveButtonEnabled(bool val);
    void setOpenButtonEnabled(bool val);
    void setLineEditEnabled(bool val);
    void setSaveButtonDisabled(bool val);
    void setOpenButtonDisabled(bool val);
    void setLineEditDisabled(bool val);

private slots:
    void onSaveButtonClicked();
    void onOpenButtonClicked();

private:
    Ui::OFileWidget *ui;
    QString openFileCaption;
    QString saveFileCaption;
    QString openFileDir;
    QString saveFileDir;
    QString openFileFilter;
    QString saveFileFilter;
    QFileDialog::Option openFileOption;
    QFileDialog::Option saveFileOption;
};

#endif // OFILEWIDGET_H
