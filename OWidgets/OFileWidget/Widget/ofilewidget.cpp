#include "ofilewidget.h"
#include "ui_ofilewidget.h"

/*!
\page index
\title Title of the page

Contents of the page
*/

/*!
    \class OFileWidget
    \brief The QVector3D class represents a vector or vertex in 3D space.
    \since 4.6
    \ingroup painting-3D

    Vectors are one of the main building blocks of 3D representation and
    drawing.  They consist of three coordinates, traditionally called
    x, y, and z.

    The QVector3D class can also be used to represent vertices in 3D space.
    We therefore do not need to provide a separate vertex class.

    \b{Note:} By design values in the QVector3D instance are stored as \c float.
    This means that on platforms where the \c qreal arguments to QVector3D
    functions are represented by \c double values, it is possible to
    lose precision.
*/
OFileWidget::OFileWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OFileWidget),
    openFileCaption("Open File"),
    saveFileCaption("Save File"),
    openFileDir("."),
    saveFileDir("."),
    openFileFilter(),
    saveFileFilter(),
    openFileOption(),
    saveFileOption()
{
    ui->setupUi(this);
    connect(ui->pushButtonOpen, &QPushButton::clicked, this, &OFileWidget::onOpenButtonClicked);
    connect(ui->pushButtonSave, &QPushButton::clicked, this, &OFileWidget::onSaveButtonClicked);
}

OFileWidget::~OFileWidget()
{
    delete ui;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//------------------Private Slots----------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//

/**
 * @brief OFileWidget::onSaveButtonClicked
 */
void OFileWidget::onSaveButtonClicked()
{
    QString selectedFilter = "";
    QString path = QFileDialog::getSaveFileName(this, saveFileCaption, saveFileDir, saveFileFilter, &selectedFilter, saveFileOption);
    ui->lineEdit->setText(path);
    emit onSaveClicked(path);
}

void OFileWidget::onOpenButtonClicked()
{
    QString selectedFilter = "";
    QString path = QFileDialog::getOpenFileName(this, openFileCaption, openFileDir, openFileFilter, &selectedFilter, openFileOption);
    ui->lineEdit->setText(path);
    emit onOpenClicked(path);
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//---------------------Setters-------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//

void OFileWidget::setLabel(const QString &text)
{
    ui->label->setText(text);
}

void OFileWidget::setPath(const QString &text)
{
    ui->lineEdit->setText(text);
}

void OFileWidget::setSaveButtonText(const QString &text)
{
    ui->pushButtonSave->setText(text);
}

void OFileWidget::setOpenButtonText(const QString &text)
{
    ui->pushButtonOpen->setText(text);
}

void OFileWidget::setSaveFileOption(const QFileDialog::Option &value)
{
    saveFileOption = value;
}

void OFileWidget::setOpenFileOption(const QFileDialog::Option &value)
{
    openFileOption = value;
}

void OFileWidget::setOpenFileFilter(const QString &value)
{
    openFileFilter = value;
}

void OFileWidget::setSaveFileDir(const QString &value)
{
    saveFileDir = value;
}

void OFileWidget::setOpenFileDir(const QString &value)
{
    openFileDir = value;
}

void OFileWidget::setSaveFileCaption(const QString &value)
{
    saveFileCaption = value;
}

void OFileWidget::setOpenFileCaption(const QString &value)
{
    openFileCaption = value;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//---------------------Getters-------------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//

QString OFileWidget::getLabel() const
{
    return ui->label->text();
}

QString OFileWidget::getPath() const
{
    return ui->lineEdit->text();
}

bool OFileWidget::isSaveButtonVisible() const
{
    return ui->pushButtonSave->isVisible();
}

bool OFileWidget::isOpenButtonVisible() const
{
    return ui->pushButtonOpen->isVisible();
}

bool OFileWidget::isLineEditVisible() const
{
    return ui->lineEdit->isVisible();
}

bool OFileWidget::isSaveButtonEnabled() const
{
    return ui->pushButtonSave->isEnabled();
}

bool OFileWidget::isOpenButtonEnabled() const
{
    return ui->pushButtonOpen->isEnabled();
}

bool OFileWidget::isLineEditEnabled() const
{
    return ui->lineEdit->isEnabled();
}

QFileDialog::Option OFileWidget::getSaveFileOption() const
{
    return saveFileOption;
}

QFileDialog::Option OFileWidget::getOpenFileOption() const
{
    return openFileOption;
}

QString OFileWidget::getOpenFileFilter() const
{
    return openFileFilter;
}

QString OFileWidget::getSaveFileDir() const
{
    return saveFileDir;
}

QString OFileWidget::getOpenFileDir() const
{
    return openFileDir;
}

QString OFileWidget::getSaveFileCaption() const
{
    return saveFileCaption;
}

QString OFileWidget::getOpenFileCaption() const
{
    return openFileCaption;
}

//-----------------------------------------------------//
//-----------------------------------------------------//
//-------------------Public SLOTS----------------------//
//-----------------------------------------------------//
//-----------------------------------------------------//

void OFileWidget::setSaveButtonVisible(bool val)
{
    ui->pushButtonSave->setVisible(val);
}

void OFileWidget::setOpenButtonVisible(bool val)
{
    ui->pushButtonOpen->setVisible(val);
}

void OFileWidget::setLineEditVisible(bool val)
{
    ui->lineEdit->setVisible(val);
}

void OFileWidget::setSaveButtonEnabled(bool val)
{
    ui->pushButtonSave->setEnabled(val);
}

void OFileWidget::setOpenButtonEnabled(bool val)
{
    ui->pushButtonOpen->setEnabled(val);
}

void OFileWidget::setLineEditEnabled(bool val)
{
    ui->lineEdit->setEnabled(val);
}

void OFileWidget::setSaveButtonDisabled(bool val)
{
    setSaveButtonEnabled(!val);
}

void OFileWidget::setOpenButtonDisabled(bool val)
{
    setOpenButtonEnabled(!val);
}

void OFileWidget::setLineEditDisabled(bool val)
{
    setLineEditEnabled(!val);
}
