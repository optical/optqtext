#include "ofilewidget.h"
#include "ofilewidgetplugin.h"

#include <QtPlugin>

OFileWidgetPlugin::OFileWidgetPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void OFileWidgetPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here
    m_initialized = true;
}

bool OFileWidgetPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *OFileWidgetPlugin::createWidget(QWidget *parent)
{
    return new OFileWidget(parent);
}

QString OFileWidgetPlugin::name() const
{
    return QLatin1String("OFileWidget");
}

QString OFileWidgetPlugin::group() const
{
    return QLatin1String("Opti Widgets");
}

QIcon OFileWidgetPlugin::icon() const
{
    return QIcon();
}

QString OFileWidgetPlugin::toolTip() const
{
    return QLatin1String("");
}

QString OFileWidgetPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool OFileWidgetPlugin::isContainer() const
{
    return false;
}

QString OFileWidgetPlugin::domXml() const
{
    return QLatin1String(
                "<ui language=\"c++\">\n"
                " <widget class=\"OFileWidget\" name=\"oFileWidget\">\n"
                "  <property name=\"geometry\">\n"
                "   <rect>\n"
                "    <x>0</x>\n"
                "    <y>0</y>\n"
                "    <width>100</width>\n"
                "    <height>20</height>\n"
                "   </rect>\n"
                "  </property>\n"
                " </widget>\n"
                "</ui>\n");
}

QString OFileWidgetPlugin::includeFile() const
{
    return QLatin1String("ofilewidget.h");
}

