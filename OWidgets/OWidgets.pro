CONFIG      += plugin debug_and_release
TARGET      = $$qtLibraryTarget(owidgetcollectionplugin)
TEMPLATE    = lib

HEADERS     = owidgetcollection.h
SOURCES     = owidgetcollection.cpp
RESOURCES   = icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS += target

include(OFileWidget/OFileWidget.pri)

DISTFILES += \
    doc/config/owidgets.qdocconf

DEFINES += OPTALL

include(../PriExtensions/optmodules.pri)
