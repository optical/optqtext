contains(DEFINES, OPTALL) {
    DEFINES += DOCUMENTATION
    DEFINES += SKELETON
    DEFINES += TRANSLATE
    DEFINES += DEPLIBS
}

include("projectinfo.pri")
include("defvariables.pri")
include("deploylibs.pri")
include("documentation.pri")
include("translate.pri")
include("skeleton.pri")
