include("defvariables.pri")

contains(DEFINES, SKELETON):!contains(DEFINES, _SKELETON_) {
    !build_pass:message("Skeleton copy is On!")
    DEFINES += _SKELETON_

    isEmpty(SRCSKELETONDIR) {
        SRC_SK_DIR = skeleton
    }
    SRC_SK_DIR = $$SRC_DIR/$$SRC_SK_DIR

    !exists($$SRC_SK_DIR) {
        !build_pass:warning("Skeleton folder is missing. Folder generated !")
        PRE_TARGETDEPS += mkSkelDir
        mkSkelDir.target = mkSkelDir
        mkSkelDir.commands = $(MKDIR) $$system_path($$SRC_SK_DIR)
        QMAKE_EXTRA_TARGETS += mkSkelDir

        ## QMake have to be rerun
        include("rerunqmake.pri")
    }

    ## Add install skeleton folder
    skeleton.target = skeleton
    skeleton.path = $$system_path($$BUILD_DIR)
    skeleton.files = $$system_path($$SRC_SK_DIR/*)

    INSTALLS += skeleton
}

