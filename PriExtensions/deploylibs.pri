include("defvariables.pri")

contains(DEFINES, DEPLIBS):!contains(DEFINES, _DEPLIBS_) {
    DEFINES += _DEPLIBS_
    !build_pass:message("Deploylibs is On!")

    win32 {
        CONFIG += windeployqt

        deployLibs.target = deployLibs
        deployLibs.path = $$BUILD_DIR
        deployLibs.files = $$README_FILE
        deployLibs.depends = windeployqt

        INSTALLS += deployLibs
    }
}
