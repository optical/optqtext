include(defvariables.pri)

contains(DEFINES, DOCUMENTATION):!contains(DEFINES, _DOCUMENTATION_) {
    !build_pass:message("Documentation On!")
    DEFINES += _DOCUMENTATION_
    DEF_DOC_DIR = $$PWD/doc

    DOC_DIR = $$SRC_DIR/doc
    CONFIG_DIR = $$DOC_DIR/config
    CONFIG_FILE = $$CONFIG_DIR/config.qdocconf

    !exists($$CONFIG_FILE) {
        ## ADD dependency on config file if not exist call copyDefDoc
        !build_pass:warning("Documentation is missing default is generated !")
        PRE_TARGETDEPS += copyDefDoc
        copyDefDoc.target = copyDefDoc
        copyDefDoc.commands = $(COPY_DIR) \"$$system_path($$DEF_DOC_DIR)\" \"$$system_path($$SRC_DIR)\"
        QMAKE_EXTRA_TARGETS += copyDefDoc


        ## QMake have to be rerun other wise default config could be overwriting existing config
        include(rerunqmake.pri)
    }

    isEmpty(QHP_FILE) {
        QHP_FILE = $$DOC_DIR/html/*.qhp
    }

    ## ADD Target make doc
    makeDoc.target = makeDoc
    makeDoc.commands = qdoc $$system_path($$CONFIG_FILE)

    ## Help Generator
    helpGen.target = helpGen
    helpGen.depends = makeDoc
    helpGen.commands = qhelpgenerator $$system_path($$DOC_DIR/html/*.qhp) -o $$system_path($$DOC_DIR/html/doc.qch)

    ## Move doc to build
    moveDoc.target = doc
    moveDoc.depends = makeDoc helpGen
    moveDoc.commands = $(MOVE) $$system_path($$DOC_DIR/html) $$system_path($$BUILD_DIR/doc)


    QMAKE_EXTRA_TARGETS += makeDoc cpyDoc helpGen
}

