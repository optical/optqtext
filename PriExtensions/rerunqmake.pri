!contains(DEFINES, QMAKE_RERUN) {
    !build_pass:message("QMake rerun On!")
    DEFINES += QMAKE_RERUN
    ## Rerun qmake
    rerunQMake.target = rerunQMake
    rerunQMake.commands = qmake $$SRC_DIR
    QMAKE_EXTRA_TARGETS += rerunQMake
    POST_TARGETDEPS += rerunQMake
}
