!contains(DEFINES, PRINFO) {
    DEFINES += PRINFO
    !build_pass:message("Project: $$TARGET")
    !build_pass:message("Template: $$TEMPLATE")
    !build_pass:message("Qt modules: $$QT")
}
