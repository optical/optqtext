TEMPLATE = lib

DISTFILES += \
    documentation.pri \
    rerunqmake.pri \
    skeleton.pri \
    defvariables.pri \
    optmodules.pri \
    projectinfo.pri \
    deploylibs.pri \
    translate.pri
