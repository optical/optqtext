!contains(DEFINES, DEFVARS) {
    !build_pass:message("Default Variables On!")
    DEFINES += DEFVARS


    BUILD_DIR = $$OUT_PWD
    SRC_DIR = $$_PRO_FILE_PWD_
    README_FILE = $$SRC_DIR/ReadMe.txt
    DEF_README_FILE = $$PWD/readme_template.txt

    !exists($$README_FILE) {
        !build_pass:warning("Readme is missing default is generated !")

        PRE_TARGETDEPS += copyDefReadme
        copyDefReadme.target = copyDefReadme
        copyDefReadme.commands = $(COPY) $$system_path($$DEF_README_FILE) $$system_path($$SRC_DIR/ReadMe.txt)
        QMAKE_EXTRA_TARGETS += copyDefReadme

        include(rerunqmake.pri)
    }

    !build_pass:message("Make pass")
    win32 {
        CONFIG(debug, debug|release) {
            STATE = "debug"
            build_pass:message("Debug pass")
        }
        CONFIG(release, debug|release) {
            STATE = "release"
            build_pass:message("Release pass")
        }
        BUILD_DIR = $$OUT_PWD/$$STATE
    }

}
