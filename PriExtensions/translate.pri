include(defvariables.pri)

contains(DEFINES, TRANSLATE):!contains(DEFINES, _TRANSLATE_) {
    !build_pass:message("Translate On!")
    DEFINES += _TRANSLATE_

    SRC_LANG_DIR = $$SRC_DIR/"langs"
    DEST_LANG_DIR = $$BUILD_DIR/"langs"

    !exists($$SRC_LANG_DIR) {
        !build_pass:warning("Src language folder is missing. Folder generated !")
        PRE_TARGETDEPS += mkSrcLangDir
        mkSrcLangDir.target = mkSrcLangDir
        mkSrcLangDir.commands = $(MKDIR) $$system_path($$SRC_LANG_DIR)
        QMAKE_EXTRA_TARGETS += mkSrcLangDir

        ## QMake have to be rerun
        include("rerunqmake.pri")
    }

    !exists($$DEST_LANG_DIR) {
        !build_pass:warning("Dest language folder is missing. Folder generated !")
        PRE_TARGETDEPS += mkDestLangDir
        mkDestLangDir.target = mkDestLangDir
        mkDestLangDir.commands = $(MKDIR) $$system_path($$DEST_LANG_DIR)
        QMAKE_EXTRA_TARGETS += mkDestLangDir

        ## QMake have to be rerun
        include("rerunqmake.pri")
    }

    exists($$SRC_LANG_DIR):!exists($$SRC_LANG_DIR/*.ts):warning("No translation files found !")
    exists($$SRC_LANG_DIR/*.ts)
    {
        trUpdate.commands = lupdate $$_PRO_FILE_

        trRelease.commands = lrelease $$_PRO_FILE_
        trRelease.depends = trUpdate

        translate.commands = $(MOVE) \"$$system_path($$SRC_LANG_DIR/)\"*.qm \"$$system_path$$DEST_LANG_DIR)\"
        translate.depends = trRelease

        QMAKE_EXTRA_TARGETS += trUpdate trRelease translate
        POST_TARGETDEPS += translate
    }
}
