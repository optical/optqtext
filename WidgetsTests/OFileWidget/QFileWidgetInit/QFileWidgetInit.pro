#-------------------------------------------------
#
# Project created by QtCreator 2015-09-24T11:23:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QFileWidgetInit
TEMPLATE = app
SOURCES += main.cpp \
    testwindow.cpp

HEADERS  += \
    testwindow.h

FORMS    += \
    testwindow.ui

