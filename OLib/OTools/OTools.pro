#-------------------------------------------------
#
# Project created by QtCreator 2015-09-29T10:11:05
#
#-------------------------------------------------

QT       -= gui

TARGET = OTools
TEMPLATE = lib
CONFIG += staticlib

SOURCES +=

HEADERS += \
    scopelambda.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include("../../PriExtensions/documentation.pri")
