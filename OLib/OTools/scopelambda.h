#ifndef SCOPELAMBDA_H
#define SCOPELAMBDA_H

namespace Opti
{
    template <typename Lambda>
    class ScopeLambda
    {
    public:
        ScopeLambda(Lambda lambda, bool active = true) :
            lambda(lambda),
            ac(active)
        {}
        void activate()
        {ac = true;}

        ~ScopeLambda()
        {if(ac) lambda();}

    private:
        Lambda &lambda;
        bool ac;
    };

    template <typename T>
    ScopeLambda<T> createScopeLambda(T lambda) {
        return ScopeLambda<T>(lambda);
    }
}

#endif // SCOPELAMBDA_H
